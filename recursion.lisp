(defun s/map (fn l)
  (if (null l) '()
    (cons (funcall fn (car l))
	  (s/map fn (cdr l)))))

(s/map (lambda (n) (* n n)) '(1 2 3 4 5))
;; => (1 4 9 16 25)

(defun s/fold (init fn l)
  (if (null l) init
    (s/fold (funcall fn init (car l)) fn (cdr l))))

(s/fold 1 (lambda (a b) (* a b)) '(1 2 3 4 5))
;; => 120

(defun s/reduce (fn l)
  (let ((third (cddr l)))
    (if (null third) (funcall fn (car l) (cadr l))
      (s/reduce fn (cons (funcall fn (car l) (cadr l))
			 (if (listp third)
			     third
			   (list third)))))))

(s/reduce #'+ '(1 2 3 4 5))
;; => 15
