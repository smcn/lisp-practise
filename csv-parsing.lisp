(defun read-csv (csv fn &key (separator ","))
  "Read CSV one line at a time and apply FN to each row. This function assumes that the first row contains the headers."
  (with-open-file (f csv)
    (let ((headers (mapcar #'parse-string (uiop:split-string (read-line f) :separator separator))))
      (loop for line = (read-line f nil :eof)
	    until (eq line :eof)
	    do (let ((row (mapcar #'cons headers (uiop:split-string line :separator separator))))
		 (funcall fn row))))))

(defun find-col (column row)
  "Search ROW (which is an alist) for COLUMN string then return the associated value."
  (when row
    (if (string= column (caar row))
	(cdar row)
	(find-col column (cdr row)))))

(defun parse-string (string)
  "Parse STRING until it can't be parsed anymore. This fixes issues where STRING could contain quotes."
  (let ((parsed-string (read-from-string string)))
    (if (not (stringp parsed-string)) string
	(parse-string parsed-string))))

;; EXAMPLE USAGE
(defun print-brand (row)
  (print (find-col "Brand" row)))

(read-csv #p"/home/user/Documents/file.csv" #'print-brand :separator "|")
