;;; REQUEST SETUP

(defclass request ()
  ((content-type
    :initarg :content-type
    :accessor content-type)
   (path
    :initarg :path
    :accessor path)
   (request-method
    :initarg :request-method
    :accessor request-method)
   (body
    :initarg :body
    :accessor body)))

(defvar *json-req* (make-instance 'request :content-type :json
					   :body "{a:1}"
					   :request-method :GET
					   :path "/products"))
(defvar *html-req* (make-instance 'request :content-type :html
					   :body "hello"
					   :request-method :POST
					   :path "/products"))

;;; FRAMEWORK

(defvar *middlewear* '())

(defmacro fail (&body body)
  `(throw :return ,@body))

(defmacro defcontroller (name middlewear &body body)
  `(defun ,name (request)
    (let ((*middlewear* (list ,@middlewear)))
      (catch :return
	(print ";; STARTING REQUEST")
	(dolist (hook *middlewear*)
	  (funcall hook request))
	,@body))))

(defmacro defmiddleware (name &body body)
  `(defun ,name (request)
     ,@body))

(defmacro route (request args)
  `(let ((req-path (path ,request))
	 (req-method (request-method ,request)))
     (cond
       ,@(mapcan
	 (lambda (arg)
	   (destructuring-bind (path method controller) arg
	     `(((and (string= req-path ,path)
		     (eq req-method ,method))
		(funcall ,controller ,request)))))
	 args)
       (t (print ";; NOT FOUND")))))

;;; USAGE

(defmiddleware only-json
  (unless (eq (content-type request) :json)
      (fail (print ";; FAILED"))))

(defcontroller products-controller (#'only-json)
  (print ";; GOT TO PRODUCTS"))

(route *html-req*
 (("/users"    :GET  nil)
  ("/products" :GET  #'products-controller)
  ("/products" :POST #'products-controller)))

(route *json-req*
 (("/users"    :GET  nil)
  ("/products" :GET  #'products-controller)
  ("/products" :POST #'products-controller)))
